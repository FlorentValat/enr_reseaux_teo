# POC ENR TEO

Ce projet rassemble tous les scripts liés à l'[**application EnR de réseaux TEO**](http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/enr_reseaux_teo/) :
- scripts de collecte, de découverte, de redressement et de datapréparation --> dossiers [collecte](collecte) + [exdata/donnees\_internes](extdata/donnees_internes),   
- et scripts de datavisualisation (application RShiny : dossier [app](app)).  

Ces différents scripts sont paramétrés pour filtrer les données nationales sur une région (ici reg=52, pour Pays de la Loire) 
et sur le dernier millesime disponible (2020).  

Voici un schéma synoptique des différents traitements :   
![](Archi_enr_teo.svg)    
  

Le package [**{COGiter}**](https://maeltheuliere.github.io/COGiter/) qui gère les référentiels de géographie administrative (communes, intercommunalités, départements...) doit être installé via devtools et github :    `devtools::install_github("MaelTheuliere/COGiter")`    
tout comme le package [**{tricky}**](http://pachevalier.github.io/tricky/) :   
`devtools::install_github("pachevalier/tricky")`.  

## Creation et utilisation de conteneur

### Au SSPcloud

L'application est deployée avec `helm` en utilisant le package [enr-deployment](https://github.com/blenzi/enr-deployment) et est accessible sur [https://enr.lab.sspcloud.fr/](https://enr.lab.sspcloud.fr/).


### En local

`docker build -t blenzi/enr .`
`docker run --rm -p 3838:3838 enr`

L'application sera disponible sur [localhost:3838](localhost:3838).

